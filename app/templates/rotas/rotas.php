<?php 
    
    include "../../controller/rotas/c_rotas.php";
?>

<!-- ./ Cadastro rota -->
<section class="wrapper-controll section-page" id="rotas">
    <span class="close icon-cancel close-page"></span>

    <div class="group clearfix pd-b">
        <h1 class="title-s db">Rotas</h1>
    </div>

    <div class="clearfix scroll">
        <?php 
            if ( mysql_num_rows($rotas) > 0 ) {
        ?>
        <ul class="list-routes">
            <?php
                while( $value = mysql_fetch_array($rotas) ) {                    
            ?>

            <li class="item" data-lat="<?php echo $value['lat']; ?>" data-log="<?php echo $value['log']; ?>" data-cli="<?php echo utf8_decode($value['cliente']); ?>" data-endereco="<?php echo utf8_decode($value['endereco']); ?>" data-cidade="<?php echo utf8_decode($value['cidade']); ?>" data-estado="<?php echo utf8_decode($value['estado']); ?>" data-data="<?php echo utf8_decode($value['data']); ?>" data-hora="<?php echo utf8_decode($value['hora']); ?>">
                <span class="icon icon-location-2"></span>
                <span class="name"><?php echo $value['cliente']; ?></span>
            </li>

            <?php
                } 
            ?>
        </ul>

        <?
            } else {
        ?>

        <p class="info pd-t">Não existem rotas cadastrados</p>  

        <?php
            }
        ?>
    </div>

    <div class="add-iten" id="show-modal-rotas">
        
    </div>
</section>