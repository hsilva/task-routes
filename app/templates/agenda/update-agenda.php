<?php 
    
    include "../../controller/agenda/c_agenda.php";
?>

<div class="content-add">
    <span class="close icon-cancel close-modal-add"></span>

    <h2 class="title-s">Atulizar Agenda</h2>

    <form class="form form-save" action="<?php echo site_url(); ?>app/controller/agenda/update_agenda.php" method="post">
        <?php 
            while ( $value = mysql_fetch_array($agendaUpdate) ) {
        ?>  

        <input type="hidden" name="id" value="<?php echo $value['id'] ?>">

        <fieldset>
            <legend>Informações Gerais</legend>

            <ul>
                <li>
                    <label for="assunto">Assunto</label>
                    <input type="text" id="assunto" name="assunto" placeholder="Assunto" value="<?php echo utf8_decode($value['assunto']); ?>">
                </li>

                <li>
                    <label for="data">Data</label>
                    <input type="text" id="data" name="data" placeholder="Data" value="<?php echo utf8_decode($value['data']); ?>">
                </li>

                <li>
                    <label for="hora">Hora</label>
                    <input type="text" id="hora" name="hora" placeholder="Hora" value="<?php echo utf8_decode($value['hora']); ?>">
                </li>

                <li>
                    <label for="cliente">Cliente</label>
                    <select name="cliente">
                        <option value="0">Cliente</option>

                        <option value="<?php echo $value['id_cliente']; ?>" selected><?php echo utf8_decode($value['nome_cliente']); ?></option>    

                        <?php 
                            while ( $c = mysql_fetch_array($cliente) ) {
                                if ( $c['id'] != $value['id_cliente'] ) {
                        ?>

                        <option value="<?php echo $c['id']; ?>"><?php echo utf8_decode($c['nome']); ?></option>    

                        <?php
                                }
                            }
                        ?>
                    </select>
                </li>

                <li>
                    <label for="produto">Produto</label>
                    <select name="produto">
                        <option value="0">Produto</option>

                        <option value="<?php echo $value['id_produto']; ?>" selected><?php echo utf8_decode($value['nome_produto']); ?></option>                            

                        <?php 
                            while ( $p = mysql_fetch_array($produto) ) {
                                if ( $p['id'] != $value['id_produto'] ) {
                        ?>

                        <option value="<?php echo $p['id']; ?>"><?php echo utf8_decode($p['nome']); ?></option>    
                        
                        <?php
                                }
                            }
                        ?>
                    </select>
                </li>

                <li>
                    <label for="quantidade">Quantidade</label>
                    <input type="text" id="quantidade" name="quantidade" placeholder="Quantidade" value="<?php echo utf8_decode($value['quantidade']); ?>">
                </li>

                <li class="descript-text">
                    <label for="descricao"></label>
                    <textarea name="descricao" placeholder="Descrição" value="<?php echo $value['descricao']; ?>"><?php echo utf8_decode($value['descricao']); ?></textarea>
                </li>

            </ul>
        </fieldset>
        
        <p class="wrap-btn db pd-t">
            <button type="submit" class="btn btn-save">Atualizar</button>
            <a href="<?php echo site_url(); ?>app/controller/agenda/delete_agenda.php" data-id="<?php echo $value['id']; ?>" class="btn btn-red btn-del">Exlcuir</a>
        </p>

        <?php 
            }
        ?>
    </form>
</div>