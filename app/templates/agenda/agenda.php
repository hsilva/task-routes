<?php 
    
    include "../../controller/agenda/c_agenda.php";
?>

<!-- ./ Cadastro Agenda -->
<section class="wrapper-controll section-page" id="agenda">
    <span class="close icon-cancel close-page"></span>

    <div class="group clearfix pd-b">
        <h1 class="title-s p-left db">Agenda</h1>

        <p class="wrap-btn db p-right">
            <a href="app/templates/agenda/add-agenda.php" class="btn btn-show-modal">Adicionar</a>
        </p>
    </div>
    
    <?php 
        if ( mysql_num_rows($agenda) > 0 ) {
    ?>

    <table class="table">
        <thead>
            <tr>
                <td>Assunto</td>
                <td>Data</td>
                <td>Cliente</td>
                <td>Excluir</td>
            </tr>
        </thead>

        <tbody>
            <?php 
                while( $value = mysql_fetch_array($agenda) ) {
            ?>

            <tr id="agenda-<?php echo $value['id']; ?>">
                <td><?php echo utf8_decode($value['assunto']); ?></td>
                <td><?php echo utf8_decode($value['data']); ?></td>
                <td><?php echo utf8_decode($value['cliente']); ?></td>
                <td><a href="app/templates/agenda/update-agenda.php?id=<?php echo $value['id']; ?>" class="btn-show-modal">Alterar</a></td>
            </tr>

            <?php
                }
            ?>
        </tbody>
    </table>

    <?
        } else {
    ?>

    <p class="info pd-t">Nenhuma informação cadastrada</p>

    <?php
        }
    ?>

    <div class="add-iten" id="show-modal-agenda">
        
    </div>
</section>