<?php 
    
    include "../../controller/agenda/c_agenda.php";
?>

<div class="content-add">
    <span class="close icon-cancel close-modal-add"></span>

    <h2 class="title-s">Adicionar Agenda</h2>

    <form class="form form-save" action="<?php echo site_url(); ?>app/controller/agenda/save_agenda.php" method="post">

        <fieldset>
            <legend>Informações Gerais</legend>

            <ul>
                <li>
                    <label for="assunto">Assunto</label>
                    <input type="text" id="assunto" name="assunto" placeholder="Assunto">
                </li>

                <li>
                    <label for="data">Data</label>
                    <input type="text" id="data" name="data" placeholder="Data">
                </li>

                <li>
                    <label for="hora">Hora</label>
                    <input type="text" id="hora" name="hora" placeholder="Hora">
                </li>

                <li>
                    <label for="cliente">Cliente</label>
                    <select name="cliente">
                        <option value="0">Cliente</option>

                        <?php 
                            while ( $value = mysql_fetch_array($cliente) ) {
                        ?>

                        <option value="<?php echo $value['id']; ?>"><?php echo utf8_decode($value['nome']); ?></option>    

                        <?php
                            }
                        ?>
                    </select>
                </li>

                <li>
                    <label for="produto">Produto</label>
                    <select name="produto">
                        <option value="">Produto</option>
                        
                        <?php 
                            while ( $value = mysql_fetch_array($produto) ) {
                        ?>

                        <option value="<?php echo $value['id']; ?>"><?php echo utf8_decode($value['nome']); ?></option>    

                        <?php
                            }
                        ?>
                    </select>
                </li>

                <li>
                    <label for="quantidade">Quantidade</label>
                    <input type="text" id="quantidade" name="quantidade" placeholder="Quantidade">
                </li>

                <li class="descript-text">
                    <label for="descricao"></label>
                    <textarea name="descricao" placeholder="Descrição"></textarea>
                </li>
            </ul>
        </fieldset>

        <p class="wrap-btn db pd-t">
            <button type="submit" class="btn btn-save">Adicionar</button>
            <button type="reset" class="btn btn-red">Cancelar</button>
        </p>
    </form>
</div>