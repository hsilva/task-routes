<?php  
    
    // Config Project
    include "../controller/config.php";

    // Controller - Home
    include "../controller/home/c_home.php";

?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Task Routes</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="classification" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [ CSS LIBS ] -->
    <link rel="stylesheet" href="<?php echo static_url(); ?>font/fontello/css/fontello.css">

    <!-- [ CSS COMMONS ] -->
    <link rel="stylesheet" href="<?php echo static_url(); ?>css/main.css">
    <link rel="stylesheet" href="<?php echo static_url(); ?>css/views/base.css">

    <!-- [ CSS VIEWS ] -->
    <link rel="stylesheet" href="<?php echo static_url(); ?>css/views/home.css">

    <script src="<?php echo static_url(); ?>js/libs/modernizr.custom.66265.js"></script>

    <script>
        window._site_url_ = '<?php echo site_url(); ?>';
        window._static_url_ = '<?php echo static_url(); ?>';
    </script>
</head>
<body>

    <div class="container">
        <header class="wrap-header">
            <div class="wrap-user">
                <h1>
                    <img src="<?php echo static_url(); ?>image/global/user.png" alt="Nome do Usuário">

                    <?php 
                        while( $value = mysql_fetch_array($queryUser) ) {
                    ?>
                    
                    <span><?php echo $value['nome']; ?> <br><?php echo $value['email']; ?></span>

                    <?php 
                        }
                    ?>
                </h1>

                <div class="wrap-config">
                    <a href="<?php echo site_url(); ?>sair/">Sair</a>    
                </div>
            </div>

            <nav class="nav-user">
                <button class="open-menu"><span class="icon icon-th-thumb-empty"></span>Menu</button>            

                <ul class="list-menu">
                    <li><a href="<?php echo site_url(); ?>app/templates/agenda/agenda.php" data-url="agenda"><span class="icon icon-map"></span> Agenda</a></li>
                    <li><a href="<?php echo site_url(); ?>app/templates/clientes/clientes.php" data-url="clientes"><span class="icon icon-user"></span> Clientes</a></li>
                    <li><a href="<?php echo site_url(); ?>app/templates/produtos/produto.php" data-url="produto"><span class="icon icon-docs"></span> Produtos</a></li>
                    <li><a href="<?php echo site_url(); ?>app/templates/rotas/rotas.php" data-url="rotas"><span class="icon icon-location-2"></span> Rotas</a></li>
                </ul>
            </nav>

            <form class="search-route" action=".">
                <label>Buscar Rotas</label>
                <input type="text" placeholder="Buscar Clientes" name="search-client">
                
                <ul class="list-cliente">
                    <?php 
                        if ( mysql_num_rows($queryClient) > 0 ) {
                            while( $value = mysql_fetch_array($queryClient) ) {
                    ?>

                        <li data-nome="<?php echo utf8_decode($value['nome']); ?>" data-latitude="<?php echo $value['latitude']; ?>" data-longitude="<?php echo $value['longitude']; ?>" data-cep="<?php echo utf8_decode($value['cep']); ?>" data-estado="<?php echo utf8_decode($value['estado']); ?>" data-cidade="<?php echo utf8_decode($value['cidade']); ?>" data-cep="<?php echo $value['cep']; ?>" data-endereco="<?php echo utf8_decode($value['endereco']); ?>" data-telefone="<?php echo $value['telefone']; ?>" data-celular="<?php echo $value['celular']; ?>">
                            <p><?php echo utf8_decode($value['nome']); ?></p>
                        </li>

                    <?php
                            }
                        } else {
                    ?>

                    <li class="not-client">
                        <p>Nenhum Cliente cadastrado</p>
                    </li>
                    
                    <?php
                        }
                    ?>
                </ul>
            </form>
        </header>

        <div class="wrap-map loading" id="map-canvas">
            <?php /* <img src="<?php echo static_url(); ?>image/content/mapa.png" alt="Mapa">  */ ?>
        </div>

        <!-- ./ Load Pages -->
        <div class="load-pages">
            <!-- Conteudo via Ajax -->
        </div>

        <div class="directions-panel">
            <span class="close-info icon-cancel"></span>

            <h2 class="title">Cliente:</h2>
            <p class="result-cli"></p>

            <h2 class="title">Data da Viagem:</h2>
            <p class="result-date">
                <span class="date"></span> - 
                <span class="hour"></span>
            </p>

            <h2 class="title">Endereço:</h2>
            <p class="result-end">
                <span class="endereco"></span>, <br>
                <span class="cidade"></span>,
                <span class="estado"></span>
            </p>

            <h2 class="title">Distãncia:</h2>
            <p class="result-distance"></p>

            <h2 class="title">Tempo Previsto:</h2>
            <p class="result-time"></p>
        </div>
    </div>

    <!-- [ JS LIBS ] -->
    <script src="<?php echo static_url(); ?>js/libs/jquery-1.11.1.min.js"></script>
    <script src="<?php echo static_url(); ?>js/libs/jquery.maskedinput.min.js"></script>
    <script src="<?php echo static_url(); ?>js/libs/jquery.maskMoney.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAdT4Tcv14DkhgHKV1FrwCE5-jbFIVO0Mk&sensor=true"></script>

    <!-- [ JS VIEWS ] -->
    <script src="<?php echo static_url(); ?>js/views/global.js"></script>
    <script src="<?php echo static_url(); ?>js/views/mapa.js"></script>

</body>
</html>