
<?php 
    
    include "../../controller/clientes/c_clientes.php";
?>

<section class="wrapper-controll section-page" id="clientes">
    <span class="close icon-cancel close-page"></span>

    <div class="group clearfix pd-b">
        <h1 class="title-s p-left db">Clientes</h1>

        <p class="wrap-btn db p-right">
            <a href="app/templates/clientes/add-clientes.php" class="btn btn-show-modal">Adicionar</a>
        </p>
    </div>

    <?php 
        if ( mysql_num_rows($query) > 0 ) {
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>Nome</td>
                <td>Email</td>
                <td>telefone</td>
                <td>Alterar</td>
            </tr>
        </thead>

        <tbody>
            <?php 
                while( $value = mysql_fetch_array($query) ) {
            ?>

            <tr id="clientes-<?php echo $value['id']; ?>">
                <td><?php echo utf8_decode($value['nome']); ?></td>
                <td><?php echo utf8_decode($value['email']); ?></td>
                <td><?php echo utf8_decode($value['telefone']); ?></td>
                <td><a href="app/templates/clientes/update-clientes.php?id=<?php echo $value['id']; ?>" class="btn-show-modal">Alterar</a></td>
            </tr>

            <?php
                }
            ?>
        </tbody>
    </table>
    <?
        } else {
    ?>

    <p class="info pd-t">Não existem clientes cadastrados</p>

    <?php
        }
    ?>

    <div class="add-iten" id="show-modal-clientes">
        
    </div>
</section>