<?php 
    include "../../controller/clientes/c_clientes.php";
?>

<div class="content-add">
    <span class="close icon-cancel close-modal-add"></span>

    <h2 class="title-s">Atualizar Cliente</h2>
    <?php 
        while( $value = mysql_fetch_array($cliente) ) {
    ?>

    <form class="form form-save" action="<?php echo site_url(); ?>app/controller/clientes/update_cliente.php" method="post">
        <input type="hidden" name="id" value="<?php echo $value['id'] ?>">
        <fieldset>
            <legend>Informações Gerais</legend>

            <ul>
                <li>
                    <label for="nome">Nome</label>
                    <input type="text" id="nome" name="nome" value="<?php echo utf8_decode($value['nome']); ?>" placeholder="Nome / Razão Social">
                </li>

                <li>
                    <label for="cpf">CPF</label>
                    <input type="text" id="cpf" name="cpf" value="<?php echo utf8_decode($value['cpf']); ?>" disabled placeholder="CPF">
                </li>

                <li>
                    <label for="cnpj">CNPJ</label>
                    <input type="text" id="cnpj" name="cnpj" value="<?php echo utf8_decode($value['cnpj']); ?>" disabled placeholder="CNPJ">
                </li>

                <li>
                    <label for="email">E-mail</label>
                    <input type="email" id="email" name="email" value="<?php echo utf8_decode($value['email']); ?>" disabled placeholder="E-mail">
                </li>

                <li>
                    <label for="telefone">Telefone</label>
                    <input type="text" id="telefone" name="telefone" value="<?php echo utf8_decode($value['telefone']); ?>" placeholder="Telefone">
                </li>

                <li>
                    <label for="celular">Celular</label>
                    <input type="text" id="celular" name="celular" value="<?php echo utf8_decode($value['celular']); ?>" placeholder="Celular">
                </li>

                <li>
                    <label for="cep">CEP</label>
                    <input type="text" id="cep" name="cep" value="<?php echo utf8_decode($value['cep']); ?>" placeholder="CEP">
                </li>

                <li>
                    <label for="endereço">Endereço</label>
                    <input type="text" id="endereco" name="endereco" value="<?php echo utf8_decode($value['endereco']); ?>" placeholder="Endereço, Nº">
                </li>

                <li>
                    <label for="cidade">Cidade</label>
                    <input type="text" id="cidade" name="cidade" value="<?php echo utf8_decode($value['cidade']); ?>" placeholder="Cidade">
                </li>

                <li>
                    <label for="estado">Estado</label>
                    <input type="text" id="estado" name="estado" value="<?php echo utf8_decode($value['estado']); ?>" placeholder="Estado">
                </li>

                <li>
                    <label for="latitude">Latitude</label>
                    <input type="text" id="latitude" name="latitude" value="<?php echo $value['latitude']; ?>" placeholder="Latitude" readonly>
                </li>

                <li>
                    <label for="longitude">Longitude</label>
                    <input type="text" id="longitude" name="longitude" value="<?php echo $value['longitude']; ?>" placeholder="Longitude" readonly>
                </li>
            </ul>
        </fieldset>

        <p class="wrap-btn db pd-t">
            <button type="submit" class="btn btn-save">Atualizar</button>
            <a href="<?php echo site_url(); ?>app/controller/clientes/delete_cliente.php" data-id="<?php echo $value['id']; ?>" class="btn btn-red btn-del">Exlcuir</a>
        </p>
    </form>

    <?php 
        }
    ?>
</div>