<?php 
    
    include "../../controller/clientes/c_clientes.php";
?>

<div class="content-add">
    <span class="close icon-cancel close-modal-add"></span>

    <h2 class="title-s">Cadastrar Cliente</h2>

    <form class="form form-save form-client" action="<?php echo site_url(); ?>app/controller/clientes/save_cliente.php" method="post">
        
        <fieldset>
            <legend>Informações Gerais</legend>

            <ul>
                <li>
                    <label for="nome">Nome</label>
                    <input type="text" id="nome" name="nome" placeholder="Nome / Razão Social">
                </li>

                <li>
                    <label for="cpf">CPF</label>
                    <input type="text" id="cpf" name="cpf" placeholder="CPF">
                </li>

                <li>
                    <label for="cnpj">CNPJ</label>
                    <input type="text" id="cnpj" name="cnpj" placeholder="CNPJ">
                </li>

                <li>
                    <label for="email">E-mail</label>
                    <input type="email" id="email" name="email" placeholder="E-mail">
                </li>

                <li>
                    <label for="telefone">Telefone</label>
                    <input type="text" id="telefone" name="telefone" placeholder="Telefone">
                </li>

                <li>
                    <label for="celular">Celular</label>
                    <input type="text" id="celular" name="celular" placeholder="Celular">
                </li>

                <li>
                    <label for="cep">CEP</label>
                    <input type="text" id="cep" name="cep" placeholder="CEP">
                </li>

                <li>
                    <label for="endereço">Endereço</label>
                    <input type="text" id="endereco" name="endereco" placeholder="Endereço, Nº">
                </li>

                <li>
                    <label for="cidade">Cidade</label>
                    <input type="text" id="cidade" name="cidade" placeholder="Cidade">
                </li>

                <li>
                    <label for="estado">Estado</label>
                    <input type="text" id="estado" name="estado" placeholder="Estado">
                </li>

                <li>
                    <label for="latitude">Latitude</label>
                    <input type="text" id="latitude" name="latitude" placeholder="Latitude" readonly>
                </li>

                <li>
                    <label for="longitude">Longitude</label>
                    <input type="text" id="longitude" name="longitude" placeholder="Longitude" readonly>
                </li>
            </ul>
        </fieldset>

        <p class="wrap-btn db pd-t">
            <button type="submit" class="btn btn-save">Adicionar</button>
            <button type="reset" class="btn btn-red">Cancelar</button>
        </p>
    </form>
</div>