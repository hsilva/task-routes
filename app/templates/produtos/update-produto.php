<?php 
    
    include "../../controller/produto/c_produto.php";
?>

<div class="content-add">
    <span class="close icon-cancel close-modal-add"></span>

    <h2 class="title-s">Atualizar Produto</h2>

    <?php 
        while( $value = mysql_fetch_array($produtoUpdate) ) {
    ?>
    <form class="form form-save" action="<?php echo site_url(); ?>app/controller/produto/update_produto.php" method="post">
        <input type="hidden" name="id" value="<?php echo $value['id']; ?>">

        <fieldset>
            <legend>Informações Gerais</legend>

            <ul>
                <li>
                    <label for="nome">Nome / Referência</label>
                    <input type="text" id="nome" name="nome" placeholder="Nome / Referência" value="<?php echo utf8_decode($value['nome']); ?>">
                </li>

                <li>
                    <label for="quantidade">Quantidade</label>
                    <input type="text" id="quantidade" name="quantidade" placeholder="Quantidade" value="<?php echo utf8_decode($value['quantidade']); ?>">
                </li>

                <li>
                    <label for="fornecedor">Fornecedor</label>
                    <input type="text" id="fornecedor" name="fornecedor" placeholder="Fornecedor" value="<?php echo utf8_decode($value['fornecedor']); ?>">
                </li>

                <li>
                    <label for="valor">Valor</label>
                    <input type="text" id="valor" name="valor" placeholder="Valor Unit." value="<?php echo utf8_decode($value['valor']); ?>">
                </li>

                <li class="descript-text">
                    <label for="descricao"></label>
                    <textarea name="descricao" placeholder="Descrição" value="<?php echo utf8_decode($value['descricao']); ?>"><?php echo utf8_decode($value['descricao']); ?></textarea>
                </li>
            </ul>
        </fieldset>

        <p class="wrap-btn db pd-t">
            <button type="submit" class="btn btn-save">Adicionar</button>
            <a href="<?php echo site_url(); ?>app/controller/produto/delete_produto.php" data-id="<?php echo $value['id']; ?>" class="btn btn-red btn-del">Exlcuir</a>
        </p>
    </form>

    <?php 
        }
    ?>    
</div>