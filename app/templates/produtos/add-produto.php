<?php 
    
    include "../../controller/produto/c_produto.php";
?>

<div class="content-add">
    <span class="close icon-cancel close-modal-add"></span>

    <h2 class="title-s">Adicionar Produto</h2>

    <form class="form form-save" action="<?php echo site_url(); ?>app/controller/produto/save_produto.php" method="post">

        <fieldset>
            <legend>Informações Gerais</legend>

            <ul>
                <li>
                    <label for="nome">Nome / Referência</label>
                    <input type="text" id="nome" name="nome" placeholder="Nome / Referência">
                </li>

                <li>
                    <label for="quantidade">Quantidade</label>
                    <input type="text" id="quantidade" name="quantidade" placeholder="Quantidade">
                </li>

                <li>
                    <label for="fornecedor">Fornecedor</label>
                    <input type="text" id="fornecedor" name="fornecedor" placeholder="Fornecedor">
                </li>

                <li>
                    <label for="valor">Valor</label>
                    <input type="text" id="valor" name="valor" placeholder="Valor Unit.">
                </li>

                <li class="descript-text">
                    <label for="descricao"></label>
                    <textarea name="descricao" placeholder="Descrição"></textarea>
                </li>
            </ul>
        </fieldset>

        <p class="wrap-btn db pd-t">
            <button type="submit" class="btn btn-save">Adicionar</button>
            <button type="reset" class="btn btn-red">Cancelar</button>
        </p>
    </form>
</div>