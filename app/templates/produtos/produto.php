<?php 
    
    include "../../controller/produto/c_produto.php";
?>

<!-- ./ Cadastro produto -->
<section class="wrapper-controll section-page" id="produto">
    <span class="close icon-cancel close-page"></span>

    <div class="group clearfix pd-b">
        <h1 class="title-s p-left db">Produtos</h1>

        <p class="wrap-btn db p-right">
            <a href="app/templates/produtos/add-produto.php" class="btn btn-show-modal">Adicionar</a>
        </p>
    </div>

    <?php 
        if ( mysql_num_rows($produtos) > 0 ) {
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>Nome</td>
                <td>Quantidade</td>
                <td>Valor</td>
                <td>Alterar</td>
            </tr>
        </thead>

        <tbody>
            <?php 
                while( $value = mysql_fetch_array($produtos) ) {
            ?>

            <tr id="produto-<?php echo $value['id']; ?>">
                <td><?php  echo utf8_decode($value['nome']); ?></td>
                <td><?php echo $value['quantidade']; ?></td>
                <td>R$ <?php echo $value['valor']; ?></td>
                <td><a href="app/templates/produtos/update-produto.php?id=<?php echo $value['id']; ?>" class="btn-show-modal">Alterar</a></td>
            </tr>

            <?php
                }
            ?>
        </tbody>
    </table>
    <?
        } else {
    ?>

    <p class="info pd-t">Não existem produtos cadastrados</p>

    <?php
        }
    ?>

    <div class="add-iten" id="show-modal-produto">
        
    </div>
</section>