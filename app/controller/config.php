<?php 
	// Arquivo de configuração global do site
	
	// Funcao responsavel por controlar a url do projeto
	function site_url() {

		// Retorna a url final do projeto
		return "http://localhost:8888/task-routes/";
	}

	// Funcao responsavel por controlar a url de arquivos estaticos do projeto
	function static_url() {

		// Retorna a url dos arquivos estaticos
		return "http://localhost:8888/task-routes/static/";
	}

	// Funcao responsavel por controlar a conexao com o banco de dados
	function database() {
		
		// Cria conexao com o bando de dados MySql
		$connect = mysql_connect("127.0.0.1", "root", "");
		mysql_select_db("task_routes_db", $connect);

		// Verifica a conexao com o banco
		if ( mysqli_connect_errno() ) {
		 	echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
	
?>