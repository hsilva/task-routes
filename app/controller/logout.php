<?php 
    
    // Configuracao do projeto
    include "config.php";    

    // Inicia a sessão de login do usuario
    session_start();

    session_destroy();

    unset($_SESSION['user_id']);

    header('location: '.site_url().'');
?>