App = window.App || {};

App.objects = {};

// Js Global Project

$(function() {
	var geocoder = new google.maps.Geocoder();

	// Open Menu
	var $btnOpenMenu = $('.open-menu'),
		$listOpenMenu = $('.list-menu');

	$btnOpenMenu.on('click', function( event ) {
		event.preventDefault();

		if ( $(this).hasClass('active') ) {
			$listOpenMenu.fadeOut();
			$(this).removeClass('active');
		} else {
			$listOpenMenu.fadeIn();
			$(this).addClass('active');
		}
	});


	// Open Modal
	var $linkMenu = $('.list-menu'),
		$elLoadPages = $('.load-pages'),
		$body = $('body'),
		speed = 300;

	$linkMenu.find('a').on('click', function( event ) {
		event.preventDefault();

		var url = $(this).attr('href'),
			current = $(this).attr('data-url');

		$elLoadPages.fadeIn( speed, function() {
			
			// Após animação carrega a página com ajax
			loadPage( current, url );
		});
	});

	function loadPage( current, url ) {
		$.ajax({
			type: 'get',
			url: url,
			success: function( data ) {
				// Carrega o conteudo da página e adiciona o retorno do HTML
				$elLoadPages.html( data );

				$('#'+current+'').fadeIn( speed, function() {
					// Após animação carrega a função fechar
					close( current );

					// Chama a funcao de adicinar ou alterar
					openModalAdd( current );
				});
			}
		});
	}

	function close( active )  {
		var $close = $('.close-page');

		$close.on('click', function() {
			$elLoadPages.fadeOut( speed );
			$('#'+active+'').fadeOut( speed );
		});
	}

	function openModalAdd( current ) {
		var $btnAdd = $('.btn-show-modal');

		$btnAdd.on('click', function( event ){ 
			event.preventDefault();

			var urlAdd = $(this).attr('href');
			$.ajax({
				type: 'get',
				url: window._site_url_+urlAdd,
				success: function( data ) {
					$('#show-modal-'+current+'').fadeIn( speed, function() {
						$('#show-modal-'+current+'').html( data );

						closeModalAdd( current );
						deleteValues( current );
						initPlugins();

						if ( current == 'clientes' ) {
							saveValuesClient();
						} else {
							saveValues();
						}
					});
				}
			});
		});
	}	

	function closeModalAdd( current ) {
		var $close = $('.close-modal-add');

		$close.on('click', function() {
			$('#show-modal-'+current+'').fadeOut( speed );
		});
	}

	function saveValues() {
		var $form = $('.form-save'),
			$btnSave = $('btn-save');

		$form.on('submit', function( event ) {
			event.preventDefault();
			var url = $(this).attr('action');

			if ( url ) {
				$.ajax({
					type: 'post',
					url: url,
					data: $(this).serialize(),
					success: function( data ) {
						window.alert(data);
					}
				});
			}
		});
	}

	function saveValuesClient() {
		var $form = $('.form-save'),
			$btnSave = $('btn-save');
			

		$form.on('submit', function( event ) {
			event.preventDefault();
			var url = $(this).attr('action');

			var $endereco = $form.find('input[name="endereco"]').val(),
				$estado = $form.find('input[name="estado"]').val(),
				$cidade = $form.find('input[name="cidade"]').val(),
				$cep = $form.find('input[name="cep"]').val(),
				address = $endereco+', '+$estado+', '+$cidade;
				
			geocoder.geocode( { 'address': address }, function(results, status) {
			  	if (status == google.maps.GeocoderStatus.OK) {

    				$form.find('input[name="latitude"]').val( results[0].geometry.location.lat() );
    				$form.find('input[name="longitude"]').val( results[0].geometry.location.lng() );

    				if ( url ) {
						$.ajax({
							type: 'post',
							url: url,
							data: $form.serialize(),
							success: function( data ) {
								window.alert(data);
							}
						});
					} 
			  	} else {
			  		alert('Preencha todos os campos e um endereço válido!')
			  	}
			});
		});
	}

	function deleteValues( current ) {
		var $btnDel = $('.btn-del'),
			msg = 'Tem certeza que deseja excluir o/a '+current+' da lista?';

		$btnDel.on('click', function( event ) {
			event.preventDefault();
			var url = $(this).attr('href');

			if ( confirm(msg) ) {
				$('#'+current+'-'+$(this).attr('data-id')+'').remove();
				
				$.ajax({
					type: 'post',
					url: url,
					data: {
						id: $(this).attr('data-id')
					},
					success: function( data ) {
						window.alert(data);
						$('#show-modal-'+current+'').fadeOut( speed );
					}
				});
			}
		});
	}

	function initPlugins() {
		$('input[name="celular"]').mask('(99) 9999-9999');
		$('input[name="telefone"]').mask('(99) 9999-9999');
		$('input[name="cpf"]').mask('999.999.999-99');
		$('input[name="cnpj"]').mask('99.999.999/9999-99');
		$('input[name="data"]').mask('99/99/9999');
		$('input[name="hora"]').mask('99:99');
		$('input[name="cep"]').mask('99999-999');

		$('input[name="valor"]').maskMoney();
	}
});