$(function() {
	var $form = $('.form-create-user');
		$formLogin = $('.form-login');

	$form.on('submit', $.proxy(function( event ) { event.preventDefault(); createUser() }, this ));
	$formLogin.on('submit', $.proxy(function( event ) { event.preventDefault(); loginUser() }, this ));

	function createUser() {

		var t = 0;

		$form.find('input').each(function() {
			var input = $(this);

			if ( input.val() == "" ) {
				input.addClass('error');
			} else {
				t++;
				input.removeClass('error');

				if ( t == $form.find('input').length ) {
					$.ajax({
						type: 'post',
						url: $form.attr('action'),
						data: $form.serialize(),
						success: function( data ) {
							if ( data != 'sucesso' ) {
								window.alert(data);
							} else {
								window.alert('Cadastro efetuado com sucesso!');
								window.location = window.__site_url__;
							}
						}
					});					
				}
			}

		});
	}

	function loginUser() {
		var t = 0;

		$formLogin.find('input').each(function() {
			var input = $(this);

			if ( input.val() == "" ) {
				input.addClass('error');
			} else {
				t++;
				input.removeClass('error');

				if ( t == $formLogin.find('input').length ) {
					$.ajax({
						type: 'post',
						url: $formLogin.attr('action'),
						data: $formLogin.serialize(),
						success: function( data ) {
							if ( data != 'sucesso' ) {
								window.alert(data);
							} else {
								window.location = window.__site_url__+'app/';
							}
						}
					});					
				}
			}

		});
	}
});