/*
*	Criando um objeto MapView com base na Api Google Maps v.3
*	para manipular as rotas dos clientes e
*	do usuario, de acordo com a latitude e
*	longitude de cada cliente
*/

App.objects.MapView = function() {
	// Defininado variaveis global para manipulacao
	var that = this;
	this.win = $(window);
	this.$body = $('body');
	this.$mapEl = $('#map-canvas');
	this.map;
	this.lat;
	this.log;
	
	// obejto marker, para pinar os ponto no mapa
	this.marker;
	this.markers = [];

	// Definindo latitude e longitude padrao 
	this.latlng = new google.maps.LatLng(-34.397, 150.644);

	this.geocoder;
	this.bounds = new google.maps.LatLngBounds(); 
	this.directionsDisplay = new google.maps.DirectionsRenderer();
	this.directionsService = new google.maps.DirectionsService();
	this.mapOptions = {
		center: that.latlng,
      	zoom: 8,
      	mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	// Routes
	this.$listRoutes = $('.list-routes');
	this.$panel = $('.directions-panel');
	this.$body.on('click', '.list-routes li', $.proxy( this.createRouteClient, this ));

	this.$panel.find('.close-info').on('click', function(){ 
		if ( $(this).hasClass('icon-cancel') ) {
			$(this).removeClass('icon-cancel').addClass('icon-left-open-1');

			that.$panel.animate({
				right: -that.$panel.innerWidth()
			});
		} else {
			$(this).addClass('icon-cancel').removeClass('icon-left-open-1');
			that.$panel.animate({
				right: 0
			});
		}
	});

	// Filter Clients Map
	this.$listClientsSearch = $('.list-cliente');
	this.$inputSearch = $('input[name="search-client"]');
	this.$inputSearch.on('focus', $.proxy(this.filterClinte, this)).focusout( $.proxy(this.hideSearch, this));
	this.$listClientsSearch.find('li').on('click', $.proxy(this.viewRouteClient, this));

	this.initEvents();
}

// Obejto Prototype de manipulacao
App.objects.MapView.prototype = {
	initEvents: function() {

		// Iniciando o mapa por padrao
		this.initializeMap();
	},

	initializeMap: function() {
		var that = this;

		// Criando escopo global do mapa com suas opcioes pre definidas
		this.map = new google.maps.Map(document.getElementById("map-canvas"), that.mapOptions);

		this.directionsDisplay.setMap( that.map );
		this.directionsDisplay.setOptions( { suppressMarkers: true } );

		if ( navigator.geolocation ) {
			that.getLocationUser();
		}
	},

	setAllMap: function ( map ) {
		var that = this;
		console.log( that.markers.length );

	  	for (var i = 0; i < that.markers.length; i++) {
	    	that.markers[i].setMap(map);
	    }
	},

	getLocationUser: function() {
		var that = this;

		// Verificando opcoes do navegador
		// Recupera as coordenadas do usuario a partir do ponto que se encontra
		navigator.geolocation.getCurrentPosition( function( position ) {
			// Recebe as coordenadas do usuario
			that.latlng = new google.maps.LatLng( position.coords.latitude, position.coords.longitude );
			
			// marca o ponto do usuario no mapa
			that.marker = new google.maps.Marker({
				map: that.map,
				draggable: false,
				center: that.latlng,
				zoom: 38,
				icon: ''+window._static_url_+'image/global/map-pin.png'
			});


			that.marker.setPosition( that.latlng );
			that.map.setCenter( that.latlng );

			that.$mapEl.removeClass('loading');
		});
	},

	createRouteClient: function( event ) {
		var that = this;

		var self = $(event.currentTarget),
			$distance = this.$panel.find('.result-distance'),
			$time = this.$panel.find('.result-time'),
			$cli = this.$panel.find('.result-cli'),
			$end = this.$panel.find('.result-end'),
			$date = this.$panel.find('.result-date'),
			lat = self.attr('data-lat'),
			log = self.attr('data-log'),
			client = self.attr('data-cli'),
			endereco = self.attr('data-endereco'),
			cidade = self.attr('data-cidade'),
			estado = self.attr('data-estado'),
			hora = self.attr('data-hora'),
			data = self.attr('data-data'),
			$bound = {},
			$coordCli;
		
		// Cria um novo objeto de coordenada do cliente
		$coordCli = new google.maps.LatLng( lat, log );

		// Marca o cliente no mapa
		that.marker = new google.maps.Marker({
			map: that.map,
			draggable: false,
			center: $coordCli,
			zoom: 14,
			icon: ''+window._static_url_+'image/global/map-pin-client.png'
		});

		this.markers.push(that.marker);

		that.marker.setPosition( $coordCli );
		 
		// Cria um objeto de conteudo para buscar a rota
		var request = {
			origin: that.latlng,
			destination: $coordCli,
			unitSystem: google.maps.UnitSystem.METRIC,
			provideRouteAlternatives: true,
			travelMode: google.maps.TravelMode.DRIVING
		};
		 
	   	that.directionsService.route( request, function( result, status ) {
			if ( status == google.maps.DirectionsStatus.OK ) {

				// Exibe no mapa a rota traçada
	         	that.directionsDisplay.setDirections( result );

	         	// Fecha o modal e Menu
	         	$('#rotas').find('.close').trigger('click');
	         	$('.open-menu').trigger('click');

	         	// crio o objeto de rota do retorno da busca
	         	var route = result.routes[0];

	         	that.$panel.show().animate({
	         		right: '0'	         	
	         	});

	         	that.$panel.addClass('active');

	         	// Pego informacoes de viagens do resultado da rota
			    for (var i = 0; i < route.legs.length; i++) {

			        $distance.text( route.legs[i].distance.text );
			        $time.text( route.legs[i].duration.text );
			        $cli.text( client );
			        $end.find('.endereco').text( endereco );
			        $end.find('.cidade').text( cidade );
			        $end.find('.estado').text( estado );
			        $date.find('.date').text( data );
			        $date.find('.hour').text( hora );
			    }

			    // Centraliza a rota de acordo com a coordenada do cliente
			    that.map.fitBounds( $coordCli );

			    that.setAllMap(null);

			} else {

			}
		});

	   	this.directionsDisplay.setPanel();
	},

	filterClinte: function( event ) {
		var that = this,
			self = $(event.currentTarget),
			$anchors =  this.$listClientsSearch.find('li');

		self.on('keyup', function() {
		    var val = self.val();		    
		    $anchors.show();
		    that.$listClientsSearch.fadeIn();

		    if ( val !== "" ) {
		        var pattern = new RegExp('^' + val, 'i');

		        $anchors.not(function( index ) {
		            return $(this).find('p').text().match(pattern);
		        }).hide();
		    }
		});
	},

	hideSearch: function( event ) {
		this.$listClientsSearch.fadeOut();
	},

	viewRouteClient: function( event ) {
		var that = this,
			self = $(event.currentTarget),
			lat = self.attr('data-latitude'),
			log = self.attr('data-longitude'),

			nome = self.attr('data-nome'),
			endereco = self.attr('data-endereco'),
			estado = self.attr('data-estado'),
			cidade = self.attr('data-cidade'),
			cep = self.attr('data-cep'),
			telefone = self.attr('data-telefone'),
			celular = self.attr('data-celular');

		// Remove todos os marcadores do mapa

		this.setAllMap(null);
		this.directionsDisplay.setDirections({routes: []});

		if ( that.$panel.hasClass('active') ) {

			this.$panel.find('.close-info').addClass('icon-cancel').removeClass('icon-left-open-1');
			that.$panel.animate({
				right: 0
			});
		}

		// Cria um novo objeto de coordenada do cliente
		var $coordCli = new google.maps.LatLng( lat, log );

		var contentString = '<div class="info-window">' +
								'<h3 class="name">'+nome+'</h3>' +
								'<p>'+endereco+ '<br>' +
								''+cidade+', '+estado+'<br>' +
								'CEP: '+cep+'</p>' +
								'<p class="sub-title">Contato</p>'+
								'<p> Telefone: '+telefone+'<br>' +
								'Celular: '+celular+'</p>' +
							'</div>';


		// Marca o cliente no mapa
		this.marker = new google.maps.Marker({
			map: that.map,
			draggable: false,
			center: $coordCli,
			zoom: 14,
			icon: ''+window._static_url_+'image/global/map-pin-client.png'
		});

		this.markers.push(that.marker);

		this.marker.setPosition( $coordCli );
		this.map.setCenter( $coordCli );

		// Cria um Web Components de Marcador com balao de informacao
		var infowindow = new google.maps.InfoWindow({
	      	content: contentString
	  	});

		// Adiciona evento ao Web Components
		google.maps.event.addListener(that.marker, 'click', function() {
			infowindow.open(that.map, that.marker);
		});
	}
}

$(function() {

	// Estancia do objeto MapView
	App.objects.mapView = new App.objects.MapView();
});
