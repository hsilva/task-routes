
<?php 
    // Config Project
    include "app/controller/config.php";
?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Task Routes -  Criar conta</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="classification" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [ CSS LIBS ] -->

    <!-- [ CSS COMMONS ] -->
    <link rel="stylesheet" href="<?php echo static_url(); ?>css/main.css">
    <link rel="stylesheet" href="<?php echo static_url(); ?>css/views/base.css">
    <link rel="stylesheet" href="<?php echo static_url(); ?>css/views/login.css">

    <!-- [ CSS VIEWS ] -->
    <link rel="stylesheet" href="<?php echo static_url(); ?>css/views/home.css">

    <script src="<?php echo static_url(); ?>js/libs/modernizr.custom.66265.js"></script>

    <script type="text/javascript">
        window.__site_url__ = '<?php echo site_url(); ?>';
    </script>   
</head>
<body>

    <section class="wrap-login wrapper"> 
        <h1 class="ir logo">Route Task</h1>

        <form class="form form-create-user" action="<?php echo site_url(); ?>app/controller/c_create_user.php" method="post">
            <h2 class="title text-center">Criar conta no Task Routes</h2>
            <ul>
                <li>
                    <label class="ir">Nome</label>
                    <input type="text" placeholder="Nome" name="nome">
                </li>

                <li>
                    <label class="ir">Email</label>
                    <input type="email" placeholder="Email" name="email">
                </li>

                <li>
                    <label class="ir">Senha</label>
                    <input type="password" placeholder="Senha" name="senha">
                </li>

                <li>
                    <input type="submit" value="entrar">
                </li>

                <li>
                    <a href="<?php echo site_url(); ?>">Login</a>
                </li>
            </ul>
        </form>
    </section>

    <!-- [ JS LIBS ] -->
    <script src="<?php echo static_url(); ?>js/libs/jquery-1.11.1.min.js"></script>

    <!-- [ JS VIEWS ] -->
    <script src="<?php echo static_url(); ?>js/views/login.js"></script>

</body>
</html>